import { ShoppingListService } from './../shopping-list.service';
import { Ingredient } from './../../shared/ingredient.model';
import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-shopping-edit',
  templateUrl: './shopping-edit.component.html',
  styleUrls: ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit {

  constructor( private shoppingListService: ShoppingListService) { }

  @ViewChild('nameInput') ingredientName: ElementRef
  @ViewChild('amountInput') ingredientAmount: ElementRef


  ngOnInit() {
  
  }

  onIngredientAdded(){
   
    console.log('ingredientAdded Emitted');
    console.log('From shopping edit: ' + this.ingredientName.nativeElement.value + ' ' + this.ingredientAmount.nativeElement.value)
    this.shoppingListService.addIngredient( {name: this.ingredientName.nativeElement.value, amount: this.ingredientAmount.nativeElement.value});

  }

}
