import { RecipesService } from './../recipes.service';
import { ShoppingListService } from './../../shopping-list/shopping-list.service';
import { Recipe } from './../recipe.model';
import { Component, OnInit, Output } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
  recipe: Recipe;
  id: number;
  ngOnInit(){
    this.route.params
      .subscribe(
        (params) =>{
          this.id = params = params['id'];
          this.recipe = this.recipesService.getRecipe(this.id)
        }
      )
  }

  constructor(private recipesService: RecipesService,
    private route: ActivatedRoute,
    private slService: ShoppingListService) {
    
  }

  onAddToSL(){
    this.slService.addIngredients(this.recipe);
  }



}
