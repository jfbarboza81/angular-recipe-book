import { EventEmitter } from '@angular/core';
import { Ingredient } from 'app/shared/ingredient.model';
import { Recipe } from './recipe.model';

export class RecipesService{

    recipeSelected = new EventEmitter<Recipe>();

    private recipes: Recipe[] = [
        new Recipe(
            'Arepas', 
            'Venezuelan corn meal', 
            'https://i0.wp.com/pursuitsandpalates.com/wp-content/uploads/2020/06/Untitled_Artwork.jpeg',
            [
                new Ingredient('Taza de Harina Pan', 1),
                new Ingredient('Tasa de Agua', 1)
            ] ),
        new Recipe(
            'Tequeños',
            'Delicious cheese sticks',
            'https://i.pinimg.com/originals/f4/7a/5e/f47a5e4655dbcd01a4b6955f8241a384.jpg',
            [
                new Ingredient('Taza de Harina de Trigo', 1),
                new Ingredient('Kilo de queso semiduro', 1)
            ] )
      ]; 
      
    getRecipes(){
        return this.recipes.slice();
    }

    getRecipe(index: number){
        return this.recipes[index];
    }

}